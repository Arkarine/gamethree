package org.gamethree.playerclient.agent;

/**
 * Behavior of the player. Implemented as an interface as there may exist different approaches for playing the game.
 * For example, a bot implementation an a real human player implementation with taking moves from the command line, etc.
 */
public interface PlayerAgentInterface {

    String getName();

    /**
     * provide the first move in the game
     * @return first move
     */
    int makeFirstMove();

    /**
     * provide a move based on the opponents move
     * @param input - opponents move
     * @return a new move
     */
    int makeMove(int input);
}
