package org.gamethree.playerclient.adapter.nats;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import org.gamethree.playerclient.adapter.EndGameException;
import org.gamethree.playerclient.adapter.GameConnectionAdapterInterface;
import org.gamethree.playerclient.adapter.MoveSubscriptionInterface;
import org.gamethree.playerclient.adapter.StartGameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * An implementation via Nats messenger. It should not be instantiated while testing.
 */
@Service
@Profile("!test")
public class NATSGameConnectionAdapter implements GameConnectionAdapterInterface {

    // The four fields are taken from the props
    private final String endGameSignal;
    private final String startGameSignal;
    private final String clientReadySubject;
    private final String playerName;

    private final Connection nats;

    @Autowired
    public NATSGameConnectionAdapter(Connection nats,
                                     @Value("${nats.ready_player_clients_topic}") String clientReadySubject,
                                     @Value("${gamethree.agent.name}") String playerName,
                                     @Value("${gamethree.nats.connection.end_game_signal}") String endGameSignal,
                                     @Value("${gamethree.nats.connection.start_game_signal}") String startGameSignal) {
        this.endGameSignal = endGameSignal;
        this.startGameSignal = startGameSignal;
        this.nats = nats;
        this.clientReadySubject = clientReadySubject;
        this.playerName = playerName;
    }

    @Override
    public void publishClientReady(String serverSubject, String replyToSubject) {
        String message = playerName + "@" + serverSubject;

        nats.publish(clientReadySubject, replyToSubject, message.getBytes());
    }

    @Override
    public void publishMove(String subjectName, int move) {
        nats.publish(subjectName, Integer.toString(move).getBytes());
    }

    @Override
    public MoveSubscriptionInterface createSubscription(String subjectName, MoveListener moveListener, ExceptionListener exceptionListener) {
        Dispatcher d = nats.createDispatcher(msg -> onMoveCame(msg, moveListener, exceptionListener));

        d.subscribe(subjectName);
        return new NATSMoveSubscription(d, nats);
    }

    @Override
    public void close() {

    }

    void onMoveCame(Message msg, MoveListener moveListener, ExceptionListener exceptionListener) {
        String message = new String(msg.getData());

        if (message.equals(startGameSignal)) {
            exceptionListener.exceptionEvent(new StartGameException());
            return;
        }

        if (message.equals(endGameSignal)) {
            exceptionListener.exceptionEvent(new EndGameException());
            return;
        }

        moveListener.moveEvent(Integer.parseInt(message));
    }


}
