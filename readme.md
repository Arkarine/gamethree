# README #
### Description ###

The project consists of two parts, the Player Client part and the Game Server part. Speaking briefly, the business logic of the Client is receiving moves 
of the opponent from the server, making moves according to them and sending back to the Server. Also, the Client makes the first move of the game or cancels
execution, if requested from the Server.  The Server business logic is receiving moves from the players (Clients), validating them and sending to an opposite 
player. To start the environment, one have to run the Nats message broker, then run a Server instance and after that run as many Client instances as wanted,
since the Server instance runs as many games as needed for a certain number of connected Clients. For example, one can run 4 or 6 Client instances, thus 2
and correspondingly 3 games will get started within the Server instance asynchronously. After a game is over, the Server sends the ending signal to the
two Clients to finish the game and stop execution.

### Flow ###
1. The broker runs and never shuts down.
2. A Server instance runs and never shuts down. Once the instance is up, it starts listening for Clients ready for game. There is a blocking concurrent queue
for keeping incoming Clients data and start games between the pairs of them. Once the pair is found, thus the game
starts. The game randomly decides who is the first and sends the starting signal to the Client who should move first. Once the game is over, the Server
sends the ending signal to the both Clients and close everything concerning the game (subscriptions, threads etc.). Also the game shuts down forcibly after some time.
3. Client instance runs and immediately sends the ready signal to the Server, thus the Server saves it and keeps until the opponent Client connects to the Server.
Once the game starts, the Client receives the starting signal (that means, the Client should do first move) or the move of the opponent. Then the Client
makes its move and sends to the Server. And so on. The client end up with the ending signal from the Server or after some time automatically.

### Run Environment ###
The projects (Client and Server) have been done on Spring Boot. One can easily open these in Intellij Idea, for instance, and Gradle will download dependencies.

* Install Docker
* Run the messenger as follows: 
```console
docker run -p 4222:4222 -p 8222:8222 -p 6222:6222 --name gnatsd -ti nats:latest
```
* Run Server instance (the easiest way is to do it straight from Intellij IDEA or another IDE)
* Run as many Client instances as needed.
