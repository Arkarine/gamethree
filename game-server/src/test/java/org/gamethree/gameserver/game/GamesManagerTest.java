package org.gamethree.gameserver.game;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GamesManagerTest {

    @Mock
    private TaskExecutor taskExecutor;
    @Mock
    private ApplicationContext applicationContext;

    private GamesManager gamesManager;

    @Before
    public void init() {
        gamesManager = spy(new GamesManager(taskExecutor, applicationContext));
    }

    @Test
    public void constructorTest() {
        BlockingQueue playersToStartQueue = (BlockingQueue) ReflectionTestUtils.getField(gamesManager, "playersToStartQueue");
        assertNotNull(playersToStartQueue);
    }

    @Test
    public void testPrepareNewGame() throws InterruptedException {
        Game newGame = mock(Game.class);
        when(applicationContext.getBean(Game.class)).thenReturn(newGame);

        PlayerClientEvent playerClientEvent1 = new PlayerClientEvent(this, "player1", "client1", "server1");
        PlayerClientEvent playerClientEvent2 = new PlayerClientEvent(this, "player2", "client2", "server2");

        BlockingQueue playersToStartQueue = (BlockingQueue) ReflectionTestUtils.getField(gamesManager, "playersToStartQueue");

        playersToStartQueue.offer(playerClientEvent1);
        playersToStartQueue.offer(playerClientEvent2);

        Game game = gamesManager.prepareNewGame();

        assertEquals(game, newGame);
        verify(game).init(playerClientEvent1, playerClientEvent2);
    }

}
