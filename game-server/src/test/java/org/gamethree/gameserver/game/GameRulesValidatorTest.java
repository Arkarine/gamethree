package org.gamethree.gameserver.game;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameRulesValidatorTest {

    private GameRulesValidator rulesValidator;

    @Before
    public void init() {
        rulesValidator = new GameRulesValidator();
    }

    @Test
    public void testValidateSingleMove() {
        assertFalse(rulesValidator.validateSingleMove(-10));
        assertFalse(rulesValidator.validateSingleMove(0));
        assertFalse(rulesValidator.validateSingleMove(1));

        assertTrue(rulesValidator.validateSingleMove(2));
        assertTrue(rulesValidator.validateSingleMove(10));
        assertTrue(rulesValidator.validateSingleMove(100000));
    }

    @Test
    public void testValidateNextMove() {
        assertFalse(rulesValidator.validateNextMove(-10, 0));
        assertFalse(rulesValidator.validateNextMove(-10, -3));
        assertFalse(rulesValidator.validateNextMove(-10, -4));

        assertTrue(rulesValidator.validateNextMove(2, 1));
        assertTrue(rulesValidator.validateNextMove(3, 1));
        assertTrue(rulesValidator.validateNextMove(4, 1));

        assertTrue(rulesValidator.validateNextMove(5, 2));
        assertTrue(rulesValidator.validateNextMove(6, 2));
        assertTrue(rulesValidator.validateNextMove(7, 2));

        assertTrue(rulesValidator.validateNextMove(50, 17));
        assertTrue(rulesValidator.validateNextMove(51, 17));
        assertTrue(rulesValidator.validateNextMove(52, 17));

        assertFalse(rulesValidator.validateNextMove(50, 18));
        assertFalse(rulesValidator.validateNextMove(51, 18));
        assertFalse(rulesValidator.validateNextMove(52, 18));

    }

    @Test
    public void testIsWinningMove() {
        assertFalse(rulesValidator.isWinningMove(-10));
        assertTrue(rulesValidator.isWinningMove(1));
        assertFalse(rulesValidator.isWinningMove(2));
        assertFalse(rulesValidator.isWinningMove(7));
    }
}
