package org.gamethree.gameserver.config;

import io.nats.client.Connection;
import io.nats.client.Nats;
import org.gamethree.gameserver.controller.NATSPlayerClientReadyController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@Configuration
@Profile("!test")
public class NatsConfig {

    private Connection natsConnection;

    @Value("${nats.ready_player_clients_topic}")
    String natsReadyClientsTopic;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Bean
    public Connection natsConnection() throws IOException, InterruptedException {
        return Nats.connect();
    }

    @Bean
    public NATSPlayerClientReadyController natsPlayerClientReadyController() throws IOException, InterruptedException {
        NATSPlayerClientReadyController natsPlayerClientReadyController =
                new NATSPlayerClientReadyController(natsConnection(),
                        natsReadyClientsTopic,
                        applicationEventPublisher);
        natsPlayerClientReadyController.init();
        return natsPlayerClientReadyController;
    }

}
